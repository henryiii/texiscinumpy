\documentclass[iSciNumPy]{subfiles}

\begin{document}

\chapter{GUI interfaces}

Sometimes, you need to make your routines accessible to others. Certainly, using descriptive names for objects, using the documentation system, and logically structuring your programmatic pieces helps, but this is not enough to hand to someone who is using to clicking buttons. Also, you may need to do a certain task over and over, or you may need to come back to an old program after a year or two and run it. These all are good reasons to build GUIs. In this chapter, I'll cover some basics of GUI design, just in case you need it. I'm going to assume that you have a already have a nice, systematic, well documented system that needs a GUI added to it. This should be one of the last steps in finishing scientific analysis programming.

I'll cover two methods. A super simple method that can be used to quickly add dialogs to a program, and the powerful Qt toolkit.


\section{Easygui}

Easy gui is a single Python file that presents a variety of simple TKinter dialogs by calling functions. You just have to call a function, and store the result. It's nearly as easy as Python's own \py{raw_input} function. \ppp{\py{input}}

\section{Qt}

The powerful, easy to use Qt framework is a great way to program a GUI. It is only marginally more complicated to do the same things easygui can do, and you can do so much more.

\subsection{Introduction to Qt}

\subsubsection{History of the two bindings}
First, why use Qt? There are so many options for GUI programming out there, and you may be especially enticed by the `built-in'\footnote{TKinter is supposed to be part of the standard library, but on many python distributions, it is either missing or partially missing so you have to add a package to use it.} TKinter framework in Python. Don't be. Qt is much more polished, up to date, and powerful than TKinter. It is available for both Python 2 and 3 and it has an amazing GUI designer. It is completely compatible with the C++ version, so that you can move to or from C++ Qt without much change. You can even look up Qt tutorials in C++ and solve you Python Qt problem with them. There is lots of documentation out there, many common tasks are already included for you, and you can customize Qt to the point of drawing your own widgets, in Python. Maybe another toolkit like wxPython is this good, but I doubt it, and I know TKinter is not this good.

So, what is Qt? We need to cover a little background now so that you don't get confused by the options later. Qt (Officially pronounced `cute', unofficially pronounced `cutie' by some people, including me) was developed by Nokia as a C++ toolkit that would be easy to bind to other languages. A company, Riverbank Software, built a Python binding for Qt called PyQt. By Qt version 4, they released it under a GPL/Commercial license. This meant that a developer using PyQt either had to make his source code available, or pay for a commercial license. Nokia wasn't happy with that, so they started their own binding set, fully open sourced and with community involvement, called PySide. PySide was released under a LGPL license, which allows you to use the library without providing all your source code\footnote{There are stipulations, see the license details if this interests you}. Not long after, though, Nokia announced that they were going to go to Microsoft technology (this is when they started moving to Windows phones), and they sold Qt. Now called the Qt Project, open source and funded development are rapidly advancing. While Qt 5 was just released, Qt 4 is the version the Python bindings use. Many of the improvements in Qt 5 are based on mobile and games.

So, there are two bindings. Since they are primarily binding to the exact same toolkit, there are very few differences. The differences come in the Python portions. A few parts of the bindings are rather odd, so new python versions were written. To indicate that it is Python only, the toolkits tend to do something like prefix the names with pyqt. Which is the major difference between the two. Most code can be changed from one binding to the other just by changing the import statement at the top, and renaming a few of these special cases.

I choose to use PyQt, because that's what comes with Python(x,y). I've also used PySide in the past. Choose either one, just remember that a few things may be different. I'll try to point out differences.

\subsubsection{Tools provided}

There are some excellent tools in the Qt Toolkit. You'll have them even if you installed Python(x,y). Qt Coder is a powerful C++ IDE that looks nice, though I don't have much experience with it. 

Qt Designer is an amazing graphical tool. It allows you make a GUI, from main windows to dialogs, even widgets, with full control over layout and resizing. It is fast once you learn it. It is powerful, even simple actions and tab order editing can be done there. Even if you don't want to use it to make a final dialog, it is a great way to learn the property names and the effect of setting things. And, once you make one, the GUI can be turned into clean, beautiful Python or be loaded dynamically from the saved Qt Designer file! The Python generator is great for learning how to make a dialog, but for production work I use the dynamic loading, since it is easy to edit the GUI in the Designer and then have the change take affect when the program is started without any file generation. Also keeps directories cleaner.

There are a few other nice tools, like the Qt Linguist, which helps in multilingual GUI's, and lots of documentation and demos. These aren't likely to be of interest, though, to a numerical scientist (at least, not to  me), so I'll leave this up to you to seek out if you really need it.

\subsection{Getting started with Qt}

Before I start, let me be the first to point out that there are several different ways to program a GUI in Qt. You don't have to use inheritance like I do; I just think that it is a good way, and I prefer to show you one way well vs. showing you multiple ways confusingly. This works for small and large programs, so that what I chose.

There are a few quirks to using Qt that need to be explained. First, it is a huge toolkit, so it is organized into many modules and sub-modules. You will primarily need two or three of them. Second, it is heavily object oriented, so you'll need to be comfortable with classes (unless you really never go beyond the easygui level). Third, it needs to be `started' before using, so there's always going go be a line at the beginning of you code that starts up the back-end of Qt. Nothing pops up, nothing special is shown; it's just `ready to work' after that runs. Fourth, you need to learn about signals and slots. This is Qt specific (most other GUIs use callbacks instead), but once you get the hang of it, it'll be great in larger apps. Finally, you might have to learn a little threading. It's not as intense as handling it in TKinter, but you may need some. I'll start with the basics.

\subsection{Terminology}

Let's cover GUI and Qt terms briefly. This might be a good reference to return to if I confuse you later.
\begin{description}
\item[Main Window] This is the main program.
\item[Widget] An object that has a specific function. A button is a widget. A text edit box is a widget. Several widgets can be grouped together to form a widget, too.
\item[Dialog] A small pop-up that asks for something or tells you something. Usually has an OK button. Closing it does not necessarily mean the program is over.
\item[Signal] Something that can `emit' when something happens. See Signals and Slots.
\item[Slot] Like a function, can run when it sees a connected signal. See Signals and Slots.
\item[Layout] A group of objects that are in a certain pattern.
\item[Parent] For a widget or a layout, this is the widow or dialog or layout or widget that it is inside. For a dialog, this is the window that this pops up in front of and usually blocks.
\end{description}

\subsubsection{First lines in Qt}

Let me start with a template for a Qt program. It's not going to do anything, but it's a useful starting point.

\begin{pyshort}
import sys
from PyQt4 import PyGui, PyCore # Optional: uic

# Program classes go here

def main():
	app = QtGui.QApplication(sys.argv)
	# Run program (or dialogs) here
	
if __name__ == '__main__':
    main()
\end{pyshort}

\begin{explaincode}
\item[2] We'll need the \py{PyGui} and \py{PyCore} modules in most programs. \py{PyCore} has lots of basic stuff, like threading.
\item[4] This is a where you'll be putting lots of class definitions for larger GUIs.
\item[7] This should go at the beginning of all programs with Qt. If you always put it in the main function of every module, you can be assured it will run once, when the app runs, regardless of which module you run. The variable \py{app} may be unused, don't worry if that happens.
\item[8] Now you can use simple dialogs, or your apps, and they will run.
\end{explaincode}

If you want to test Qt commands on the command line, or use auto-completion in your favorite editor (or manually call \py{dir}), you will need to run lines 1, 2, and 7 first.


\end{document}