\documentclass[iSciNumPy]{subfiles}

\begin{document}


\chapter{Dive into programming with classes}
Here I'm going to show you a few samples of programming. They are going
to be object oriented examples. If you use almost any Python library (or
almost any C++ library, like Geant4), you will need these ideas. Plus,
most labs I know of are trying to move to object oriented code for
it's maintainability and extendability.

\section{Uncertainty}
\index{examples!uncertainty}

Problem statement: We want to make a new datatype (class) that can
work with numbers that have associated uncertainties.

Like all example code, this is usually best accomplished by an existing
library, such as \py{Quantities}. But it makes a good example because
you are familiar with the problem. Remember, look before you code!

I've made this one obvious. It calls for a class. I'm going to start
by writing the code that the class will be run with. (Yes, this will
not work until we write the class. But it's a good way to visualize
what we want to do). Here's the code:

\begin{pyshort}
from uncertaintymodule import Uncertainty
a_value = Uncertainty(1.00, .05)
b_value = Uncertainty(2.34, .01)
c_value = Uncertainty(12)
d_value = a_value**2 + b_value
e_value = d_value * c_value
print d_value, e_value
\end{pyshort}

Simple? Let's take a look at that, by line.

\begin{explaincode}
% Problem here!
\item[1] We load our class from our module. The module filename will be \cmd{uncertaintymodule.py}. We will call the class in it \py{Uncertainty}. We could have added \py{as unc} to shorten the very long name for the rest of the code, but I left it simple.
\item[2] We set up a new object, called \py{a_value}, with a value of \py{1.0} and an uncertainty of \py{0.05}.
\item[4] Here we omit the uncertainty, so we'll assume a default value (0). This should act like a normal int or float.
\item[5-6] We want to be able to raise an \py{Uncertainty} to a power, add them, and multiply them. These will need to work correctly for \py{Uncertainty}.
\item[7] Printing should work, so therefore str() should work, since that's what print uses in a normal shell. And, as a standard, repr() should also work, since that's how most shells show you what the object looks like when you just type it by itself.
\end{explaincode}

So, this should print two nicely printed values with uncertainties.
Let's look at building a class that does that.

\subsection{Constructor}
Let's start with the constructor.

\pyfile{6-11}{Uncertanty.py}

\begin{explaincode}
\item We'll want to take square roots, so we'll grab the standard library
function for it.
\item We define a new class with the \py{class} keyword. After the name, we put
\py{(object)} to tell Python to inherit all the goodies from the base class
\py{object}. If you don't put this part, you object will act like
an old-style Python class, and will not work as nicely. If you are
in Python 3, all classes are new classes, and you can ignore this
part. And, of course, the colon tells Python that a block is coming
(the body of the class).
\item We can define functions (methods) or set variables inside a class
(or even other classes - remember, everything is an object, and classes
contain objects). Most of the time, we'll be making functions though.
Here, we make the \py{__init__} special function. There are 30 or so
special functions, surrounded by underscores. They control how the
class works. \py{__init__} sets up the class, and is called by Python
when you make a new object. All methods in classes start with the
\py{self} variable, then all the remaining parameters/arguments are the
'visible' ones. \py{self}, for an \py{__init__} function, is a brand new
object ready to be filled with data. The other two parameters will
be seen when someone calls \py{Uncertainty(val)} or \py{Uncertainty(val,unc)}.
The equals sign makes the final parameter optional. Never put a mutable
object as the default value.
\item This is a docstring. In this special position, a string will turn
into a docstring. I won't always include them, but you should, at
least for normal functions and the \py{__init__} or \py{__call__} special
functions. Triple quotes allow me to include line breaks in the string.
Most shells will show a docstring as you type, and help generation
tools (several of which are built into Python) use them too.
\item I'm making a new ``member'', or object variable, and putting \py{val}
into it. I'm keeping the name the same just to be tidy. Avoid using
class variables; object variables are much better, and they look the
same when you use them. Class variables are the same for all class
members, which a) is probably not what you want, b) if they are immutable,
then they get replaced when you assign to them (so they may look like
an object variable), and c) mutable variables will change across all
objects. So, don't use them.
\item And, also for \py{unc}. Notice that \py{val} and \py{unc} will cease to exist when we leave \py{__init__}, but \py{self.val} and \py{self.unc} will always be there.
\end{explaincode}

\subsection{Printing}
Now, let's start filling in some features. These also need to be in
the class block (I'm just splitting up the code so I can write a lot
of stuff). Let's make our objects print nice versions of themselves
on the command line:

\pyfile{13-17}{Uncertanty.py}
When you see an object on the command line, you usually see it's \py{__repr__} representation. This is meant to be a clear but not particularly pretty way of representing the object.

This is a great version of \py{__repr__}. It displays the object
using something that looks like the constructor that would make one
just like it. If you don't like including the class name in the body
of a class, in case you want to change it, you can use \py{self.__class__.__name__} like I did.

I'm also using the powerful \py{.format} method of strings. This
is nice, and very Pythonic. It also looks very cool to call a method
on a pair of quotes, in my opinion. We could also have used C style
formatting, with the \cmd{\%f} and similar tokens, along with a \cmd{\%} operator
between the string and the arguments. I like this better.

The other way of displaying an object is with a prettier string display. This is what \py{print} uses, and is what you get if you use the \py{str} function. This is internally the \py{__str__} function. Here, I've made a nice, pretty print display.

\subsection{Operations}

\pyfile{19-32}{Uncertanty.py}

\begin{explaincode}
\item Here, I call the second object \py{other}; it's sort of a convention, but
people won't hunt you down for breaking it.
\item I'm keeping the same names \py{val} and \py{unc}. They do not conflict
with \py{self.val} or \py{other.val}. I could have used any other name, but
this was simple.
\item I'm using the \py{sqrt} function I imported earlier.
\item Here, I'm making a new object, and setting it up with the new values.
Never change the values in self or other unless it is exected! Or
other functions where that would not be expected. \py{c=a+b} should not
change \py{a} (\py{self}) or \py{b} (\py{other}), only make a new \py{c} (returned object).
In place addition (\py{b += a}) will work, but it will not be as space
and speed optimized as a function that actually does change self.
So, if you really need that, \py{__iadd__} is the inplace adding function. There
is also one thing here that should grate on your nerves. The use of
the class name in the class. Now, if I wanted to change it from \py{Uncertainty}
to something else, I'd have to change this function too (using \py{as}
in an import will not hurt it, by the way). This is a bigger problem
than you think; what happens if I inherit the class? If you don't
know what that is yet, I don't expect an answer. You can simply use
\py{self.__class__} instead of a name to solve both problems. Just
remember, \py{self.__class__} will always be the fully inherited class;
make sure this is what you want. I think it usually will be. type(self)
also works for new style classes. Some people prefer it; remembering
that type and class are almost synonymous in modern Python, either
should be fine. The \py{self.__class__} one is slightly more grammatically
correct.

Multiply and power are similar, so I'll avoid adding too many comments.
Just notice that power is expected to be a number. In following standard
Python programming, I just assume it is. You very rarely test for
the proper type. Just assume it works. Python will nicely fail if
it does not work.
\end{explaincode}

So, could we have improved this? Maybe. Looking over this for redundant
typing, you might see \py{a.unc/a.val} showing up several times. Wouldn't
it be nice to make a special way to get this commonly used value?
You'll see ``properties'' in the next example, designed for just
such a purpose. Of course, a method could do it too, but that is messier
syntax when you really want a single value that should ``exist'',
but is actually calculated from other things.

Now, we have a simple, working class for uncertainties. We could use
it in the example. Or, we could use it in a shell. Or in a program.
This solves a problem in a very generic way, and is easy to use anywhere.
Much better than a static program! That's the power of object oriented
programming. At least, the tip of the iceberg of it. :)

\subsection{Inheratance}

Let's say we were unhappy with the print function. We could edit the
class code, or we could change the class and give it a new name. That's
what is called ``inheritance''. Let's try that now:

\pyfile{34-36}{Uncertanty.py}

Now, if we used \py{self.__class__} everywhere in our previous class, this new class will work exactly like the old one, and we can forget the old one exists.
Or use both. Simple, powerful. We've also added nicer, unicode printing.
If we were in Python 3, \py{__str__} would be sufficient, we could
avoid the explicit u in front of the string, since all strings are
unicode in Python 3.

Will any of the functions break? Look at \py{__add__}. The return value is \py{Uncertainty(val, unc)}. So, even if you use the \py{NewUnc} class, you'll end back up with the old class when you add. This is probably not what you wanted, so use the convention \py{self.__class__} to refer to the current class.

\section{Vectors}

Now, let's make the class all scientific programmers probably have
made at one point. The Vector class. I'll come back to this later,
making a Numpy powered, inherited class. But for now, let's use it
to learn from. Here's a basic class, covering all the most common
uses, like adding and multiplying. I won't provide a full breakdown,
but will just point out a few of the major parts.

Notice the \py{__rmul__} special method. This allows \py{3*Vector} to
work, instead of just \py{Vector*3} and \py{Vector*Vector}. I don't
support adding scalars to vectors, so there is no need for \py{__radd__}.

It is possible to access the \py{x} value of a \py{Vector} \py{a} three ways,
\py{a.x}, \py{a[0]}, and \py{a._data[0]}. 


\end{document}