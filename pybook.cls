\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{pybook}[2012/12/19 v0.01 Commands for a python book]
% Provides the following environments:
%	pyexample - For a full example
%
% Provides the following commands
%	py - Simple snipit of Python
%	cmd - Command line txt
%	pycode - Longer snipit of Python
%	pysplit - Two column Python piece
%
%

\LoadClass[11pt]{memoir}

\RequirePackage{xparse}
\RequirePackage{etoolbox} % Bool commands (has tons of xparse like handy commands)
\RequirePackage{listings}
\RequirePackage{color}
\RequirePackage{textcomp}
\RequirePackage[usenames,dvipsnames]{xcolor}
\RequirePackage{enumitem}
\RequirePackage[hyphens]{url}
\RequirePackage{tikz}
\usetikzlibrary{shadows.blur, shapes}

\RequirePackage{amsmath} %Never write a paper without using amsmath for its many new commands 
\RequirePackage{amssymb} %Some extra symbols 
\RequirePackage{graphicx} %If you want to include postscript graphics 

\lstloadlanguages{Python}

\definecolor{ltgrey}{rgb}{.9,.9,.9}
\definecolor{codecolor}{rgb}{.2,0.,0.}
\definecolor{commandcolor}{rgb}{.0,.2,.2}
\newlength{\lengthCirc}

% Formating the page
\setlrmarginsandblock{1.25in}{1in}{*}
\checkandfixthelayout
\chapterstyle{madsen}

% Baseline is nice, but kills shadow
\newcommand*\circled[1]{%
            \settowidth{\lengthCirc}{#1}%
            %\showthe\lengthC%
            \tikz[]{%
            \node[shape=%
            \ifdimless{\lengthCirc}{10pt}{circle}{ellipse}%
            ,shade,top color=blue!40,
      bottom color=blue!5,shading angle=-135,
      blur shadow={shadow blur steps=5},
      draw,inner sep=2pt] (char) {#1};}}

\lstset{language=Python,
	showstringspaces=false,
	basicstyle=\ttfamily\color{codecolor},
	tabsize=3,
	numbers=left,
	breaklines=true,
 	numberstyle=\tiny,
 	numberblanklines=false,
 	keywordstyle=[1]\color{BurntOrange},
 	keywordstyle=[2]\color{Plum},
    commentstyle=\color{Red},
    stringstyle=\color{Green},
    columns=fullflexible,
    keepspaces=true,
    upquote=true,
    backgroundcolor=\color{ltgrey},
          }
\lstset{moredelim=[il][\rmfamily\color{Blue}]{\#\#}}

\lstset{classoffset=1,morekeywords={abs, divmod, input, open, staticmethod, all, enumerate, int, ord, str, any, eval, isinstance, pow, sum, basestring, execfile, issubclass, print, super, bin, file, iter, property, tuple, bool, filter, len, range, type, bytearray, float, list, raw_input, unichr, callable, format, locals, reduce, unicode, chr, frozenset, long, reload, vars, classmethod, getattr, map, repr, xrange, cmp, globals, max, reversed, zip, compile, hasattr, memoryview, round, __import__, complex, hash, min, set, apply, delattr, help, next, setattr, buffer, dict, hex, object, slice, coerce, dir, id, oct, sorted, intern,None,True,False,Ellipsis,NotImplemented,__debug__,Exception,StopIteration,Warning}}

\DeclareDocumentCommand \py { m }{\lstinline{#1}}
\DeclareDocumentCommand \cmd { m }{\texttt{\color{commandcolor}#1}}
\DeclareDocumentCommand \weblink { m }{\url{#1}}
\DeclareDocumentCommand \pyfile { m m }{\lstinputlisting[linerange=#1]{pyfiles/#2}}
\NewDocumentCommand \nal {} {\tabularnewline\hline}
\DeclareDocumentCommand \ppp { m }{\marginpar{P3:#1}}

\let\Olditem\item

\RenewDocumentCommand \item { o }{%
\IfNoValueTF{#1}{%
\Olditem%
}{%
\iftoggle{inexplaincode}{%
\Olditem[\protect\circled{#1}]%
}{%
\Olditem[#1]%
}}}

\newtoggle{inexplaincode}
\togglefalse{inexplaincode}

\DeclareDocumentEnvironment{explaincode}{}{%
\toggletrue{inexplaincode}%
\begin{enumerate}[label=\protect\circled{\arabic*}]%
}{%
\end{enumerate}%
\togglefalse{inexplaincode}%
}

\DeclareDocumentEnvironment{pydescription}{O{6em}}{%
\begin{description}[font=\normalfont\ttfamily\color{codecolor},leftmargin=#1,style=nextline]%
}{%
\end{description}%
}

\lstnewenvironment{pyveryshort}{\lstset{numbers=none}}{}
\lstnewenvironment{pyshort}{}{}
\lstnewenvironment{pylong}{}{}

