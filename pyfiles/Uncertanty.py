# Uncertanty.py
#  By Henry Schreiner
# Released under the GNU license
# Part of iScyNumPy

from math import sqrt
class Uncertainty(object):
    def __init__(self, val, unc=0):
        '''Create a new instance with val and unc. 0 uncertainty if not specified.'''
        self.val = val
        self.unc = unc
        
    def __repr__(self):
        return "{0}({1}, {2})".format(self.__class__.__name__,self.val, self.unc)
        
    def __str__(self):
        return '{0} +/- {1}'.format(self.val, self.unc)
        
    def __add__(self, other):
        val = self.val + other.val
        unc = sqrt( self.unc**2 + other.unc**2)
        return Uncertainty(val, unc)
        
    def __mul__(self, other):
        val = self.val * other.val
        unc = val * sqrt( (self.unc/self.val)**2 + (other.unc/other.val)**2 )
        return self.__class__(val, unc)
        
    def __pow__(self, power):
        val = self.val**power
        unc = val * self.unc/self.val * power
        return self.__class__(val, unc)
        
class NewUnc(Uncertainty):
    def __unicode__(self):
        u'{0} \u00B1 {1}'.format()